# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "speechtotxt_action: 7 messages, 0 services")

set(MSG_I_FLAGS "-Ispeechtotxt_action:/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg;-Iactionlib_msgs:/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(speechtotxt_action_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" "actionlib_msgs/GoalID:speechtotxt_action/SpeechResult:std_msgs/Header:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" "speechtotxt_action/SpeechActionGoal:speechtotxt_action/SpeechActionResult:speechtotxt_action/SpeechGoal:std_msgs/Header:speechtotxt_action/SpeechFeedback:speechtotxt_action/SpeechResult:speechtotxt_action/SpeechActionFeedback:actionlib_msgs/GoalID:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" "actionlib_msgs/GoalID:speechtotxt_action/SpeechFeedback:std_msgs/Header:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" ""
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" ""
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" ""
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_custom_target(_speechtotxt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speechtotxt_action" "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" "actionlib_msgs/GoalID:speechtotxt_action/SpeechGoal:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_cpp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
)

### Generating Services

### Generating Module File
_generate_module_cpp(speechtotxt_action
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(speechtotxt_action_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(speechtotxt_action_generate_messages speechtotxt_action_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_cpp _speechtotxt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speechtotxt_action_gencpp)
add_dependencies(speechtotxt_action_gencpp speechtotxt_action_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speechtotxt_action_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_eus(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
)

### Generating Services

### Generating Module File
_generate_module_eus(speechtotxt_action
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(speechtotxt_action_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(speechtotxt_action_generate_messages speechtotxt_action_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_eus _speechtotxt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speechtotxt_action_geneus)
add_dependencies(speechtotxt_action_geneus speechtotxt_action_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speechtotxt_action_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_lisp(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
)

### Generating Services

### Generating Module File
_generate_module_lisp(speechtotxt_action
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(speechtotxt_action_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(speechtotxt_action_generate_messages speechtotxt_action_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_lisp _speechtotxt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speechtotxt_action_genlisp)
add_dependencies(speechtotxt_action_genlisp speechtotxt_action_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speechtotxt_action_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_nodejs(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
)

### Generating Services

### Generating Module File
_generate_module_nodejs(speechtotxt_action
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(speechtotxt_action_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(speechtotxt_action_generate_messages speechtotxt_action_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_nodejs _speechtotxt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speechtotxt_action_gennodejs)
add_dependencies(speechtotxt_action_gennodejs speechtotxt_action_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speechtotxt_action_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)
_generate_msg_py(speechtotxt_action
  "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
)

### Generating Services

### Generating Module File
_generate_module_py(speechtotxt_action
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(speechtotxt_action_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(speechtotxt_action_generate_messages speechtotxt_action_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speechtotxt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speechtotxt_action_generate_messages_py _speechtotxt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speechtotxt_action_genpy)
add_dependencies(speechtotxt_action_genpy speechtotxt_action_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speechtotxt_action_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speechtotxt_action
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(speechtotxt_action_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(speechtotxt_action_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speechtotxt_action
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(speechtotxt_action_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(speechtotxt_action_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speechtotxt_action
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(speechtotxt_action_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(speechtotxt_action_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speechtotxt_action
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(speechtotxt_action_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(speechtotxt_action_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speechtotxt_action
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(speechtotxt_action_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(speechtotxt_action_generate_messages_py std_msgs_generate_messages_py)
endif()
