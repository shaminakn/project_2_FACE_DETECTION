# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "speech2txt_action: 7 messages, 0 services")

set(MSG_I_FLAGS "-Ispeech2txt_action:/home/shamina/catkin_ws/devel/share/speech2txt_action/msg;-Iactionlib_msgs:/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(speech2txt_action_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" "speech2txt_action/SpeechResult:actionlib_msgs/GoalID:std_msgs/Header:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" ""
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" ""
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" "speech2txt_action/SpeechFeedback:actionlib_msgs/GoalID:std_msgs/Header:actionlib_msgs/GoalStatus"
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" ""
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" "speech2txt_action/SpeechGoal:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" NAME_WE)
add_custom_target(_speech2txt_action_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "speech2txt_action" "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" "speech2txt_action/SpeechGoal:speech2txt_action/SpeechFeedback:std_msgs/Header:speech2txt_action/SpeechActionResult:speech2txt_action/SpeechResult:speech2txt_action/SpeechActionGoal:actionlib_msgs/GoalID:speech2txt_action/SpeechActionFeedback:actionlib_msgs/GoalStatus"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_cpp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
)

### Generating Services

### Generating Module File
_generate_module_cpp(speech2txt_action
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(speech2txt_action_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(speech2txt_action_generate_messages speech2txt_action_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_cpp _speech2txt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speech2txt_action_gencpp)
add_dependencies(speech2txt_action_gencpp speech2txt_action_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speech2txt_action_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)
_generate_msg_eus(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
)

### Generating Services

### Generating Module File
_generate_module_eus(speech2txt_action
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(speech2txt_action_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(speech2txt_action_generate_messages speech2txt_action_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_eus _speech2txt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speech2txt_action_geneus)
add_dependencies(speech2txt_action_geneus speech2txt_action_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speech2txt_action_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)
_generate_msg_lisp(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
)

### Generating Services

### Generating Module File
_generate_module_lisp(speech2txt_action
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(speech2txt_action_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(speech2txt_action_generate_messages speech2txt_action_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_lisp _speech2txt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speech2txt_action_genlisp)
add_dependencies(speech2txt_action_genlisp speech2txt_action_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speech2txt_action_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)
_generate_msg_nodejs(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
)

### Generating Services

### Generating Module File
_generate_module_nodejs(speech2txt_action
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(speech2txt_action_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(speech2txt_action_generate_messages speech2txt_action_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_nodejs _speech2txt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speech2txt_action_gennodejs)
add_dependencies(speech2txt_action_gennodejs speech2txt_action_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speech2txt_action_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)
_generate_msg_py(speech2txt_action
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg"
  "${MSG_I_FLAGS}"
  "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg;/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
)

### Generating Services

### Generating Module File
_generate_module_py(speech2txt_action
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(speech2txt_action_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(speech2txt_action_generate_messages speech2txt_action_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechResult.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionFeedback.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechActionGoal.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/shamina/catkin_ws/devel/share/speech2txt_action/msg/SpeechAction.msg" NAME_WE)
add_dependencies(speech2txt_action_generate_messages_py _speech2txt_action_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(speech2txt_action_genpy)
add_dependencies(speech2txt_action_genpy speech2txt_action_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS speech2txt_action_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/speech2txt_action
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(speech2txt_action_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(speech2txt_action_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/speech2txt_action
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(speech2txt_action_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(speech2txt_action_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/speech2txt_action
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(speech2txt_action_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(speech2txt_action_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/speech2txt_action
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(speech2txt_action_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(speech2txt_action_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/speech2txt_action
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(speech2txt_action_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(speech2txt_action_generate_messages_py std_msgs_generate_messages_py)
endif()
