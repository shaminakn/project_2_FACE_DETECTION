# CMake generated Testfile for 
# Source directory: /home/shamina/catkin_ws/src
# Build directory: /home/shamina/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(gazebo_ros_demos/rrbot_control)
subdirs(gazebo_ros_demos/rrbot_description)
subdirs(gazebo_ros_demos/rrbot_gazebo)
subdirs(project)
subdirs(beginner_tutorials)
subdirs(person_details)
subdirs(actionlib_tutorials)
subdirs(gazebo_ros_demos/custom_plugin_tutorial)
