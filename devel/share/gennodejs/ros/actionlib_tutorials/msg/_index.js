
"use strict";

let FibonacciActionResult = require('./FibonacciActionResult.js');
let speechActionGoal = require('./speechActionGoal.js');
let speechGoal = require('./speechGoal.js');
let FibonacciActionFeedback = require('./FibonacciActionFeedback.js');
let speechActionFeedback = require('./speechActionFeedback.js');
let speechActionResult = require('./speechActionResult.js');
let SpeechResult = require('./SpeechResult.js');
let FibonacciAction = require('./FibonacciAction.js');
let FibonacciResult = require('./FibonacciResult.js');
let FibonacciFeedback = require('./FibonacciFeedback.js');
let SpeechActionResult = require('./SpeechActionResult.js');
let speechAction = require('./speechAction.js');
let speechFeedback = require('./speechFeedback.js');
let SpeechAction = require('./SpeechAction.js');
let SpeechFeedback = require('./SpeechFeedback.js');
let FibonacciActionGoal = require('./FibonacciActionGoal.js');
let SpeechActionGoal = require('./SpeechActionGoal.js');
let speechResult = require('./speechResult.js');
let SpeechActionFeedback = require('./SpeechActionFeedback.js');
let SpeechGoal = require('./SpeechGoal.js');
let FibonacciGoal = require('./FibonacciGoal.js');

module.exports = {
  FibonacciActionResult: FibonacciActionResult,
  speechActionGoal: speechActionGoal,
  speechGoal: speechGoal,
  FibonacciActionFeedback: FibonacciActionFeedback,
  speechActionFeedback: speechActionFeedback,
  speechActionResult: speechActionResult,
  SpeechResult: SpeechResult,
  FibonacciAction: FibonacciAction,
  FibonacciResult: FibonacciResult,
  FibonacciFeedback: FibonacciFeedback,
  SpeechActionResult: SpeechActionResult,
  speechAction: speechAction,
  speechFeedback: speechFeedback,
  SpeechAction: SpeechAction,
  SpeechFeedback: SpeechFeedback,
  FibonacciActionGoal: FibonacciActionGoal,
  SpeechActionGoal: SpeechActionGoal,
  speechResult: speechResult,
  SpeechActionFeedback: SpeechActionFeedback,
  SpeechGoal: SpeechGoal,
  FibonacciGoal: FibonacciGoal,
};
