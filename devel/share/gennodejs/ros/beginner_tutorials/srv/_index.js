
"use strict";

let AddTwoInts = require('./AddTwoInts.js')
let SpeechToText = require('./SpeechToText.js')

module.exports = {
  AddTwoInts: AddTwoInts,
  SpeechToText: SpeechToText,
};
