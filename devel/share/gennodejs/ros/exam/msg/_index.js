
"use strict";

let PersonFeedback = require('./PersonFeedback.js');
let PersonAction = require('./PersonAction.js');
let PersonActionResult = require('./PersonActionResult.js');
let PersonGoal = require('./PersonGoal.js');
let PersonActionGoal = require('./PersonActionGoal.js');
let PersonResult = require('./PersonResult.js');
let PersonActionFeedback = require('./PersonActionFeedback.js');

module.exports = {
  PersonFeedback: PersonFeedback,
  PersonAction: PersonAction,
  PersonActionResult: PersonActionResult,
  PersonGoal: PersonGoal,
  PersonActionGoal: PersonActionGoal,
  PersonResult: PersonResult,
  PersonActionFeedback: PersonActionFeedback,
};
