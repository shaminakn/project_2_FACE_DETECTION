
"use strict";

let SpeechResult = require('./SpeechResult.js');
let SpeechActionResult = require('./SpeechActionResult.js');
let SpeechAction = require('./SpeechAction.js');
let SpeechFeedback = require('./SpeechFeedback.js');
let SpeechActionGoal = require('./SpeechActionGoal.js');
let SpeechActionFeedback = require('./SpeechActionFeedback.js');
let SpeechGoal = require('./SpeechGoal.js');

module.exports = {
  SpeechResult: SpeechResult,
  SpeechActionResult: SpeechActionResult,
  SpeechAction: SpeechAction,
  SpeechFeedback: SpeechFeedback,
  SpeechActionGoal: SpeechActionGoal,
  SpeechActionFeedback: SpeechActionFeedback,
  SpeechGoal: SpeechGoal,
};
