// Auto-generated. Do not edit!

// (in-package speech2txt_srv.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class SpeechRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.a = null;
    }
    else {
      if (initObj.hasOwnProperty('a')) {
        this.a = initObj.a
      }
      else {
        this.a = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SpeechRequest
    // Serialize message field [a]
    bufferOffset = _serializer.string(obj.a, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SpeechRequest
    let len;
    let data = new SpeechRequest(null);
    // Deserialize message field [a]
    data.a = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.a.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'speech2txt_srv/SpeechRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'cec2f53f86620c7bb01476cbe41b2fae';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string a
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SpeechRequest(null);
    if (msg.a !== undefined) {
      resolved.a = msg.a;
    }
    else {
      resolved.a = ''
    }

    return resolved;
    }
};

class SpeechResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.analysis = null;
    }
    else {
      if (initObj.hasOwnProperty('analysis')) {
        this.analysis = initObj.analysis
      }
      else {
        this.analysis = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SpeechResponse
    // Serialize message field [analysis]
    bufferOffset = _serializer.string(obj.analysis, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SpeechResponse
    let len;
    let data = new SpeechResponse(null);
    // Deserialize message field [analysis]
    data.analysis = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.analysis.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'speech2txt_srv/SpeechResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '4dff69ab0c3c84d7435a61354edd9110';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string analysis
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SpeechResponse(null);
    if (msg.analysis !== undefined) {
      resolved.analysis = msg.analysis;
    }
    else {
      resolved.analysis = ''
    }

    return resolved;
    }
};

module.exports = {
  Request: SpeechRequest,
  Response: SpeechResponse,
  md5sum() { return 'ab61c62fb399f4eebf59b55f633add2f'; },
  datatype() { return 'speech2txt_srv/Speech'; }
};
