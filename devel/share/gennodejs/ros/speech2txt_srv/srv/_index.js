
"use strict";

let Speech = require('./Speech.js')
let AddTwoInts = require('./AddTwoInts.js')

module.exports = {
  Speech: Speech,
  AddTwoInts: AddTwoInts,
};
