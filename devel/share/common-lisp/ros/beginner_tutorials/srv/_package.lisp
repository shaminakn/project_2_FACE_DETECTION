(cl:defpackage beginner_tutorials-srv
  (:use )
  (:export
   "ADDTWOINTS"
   "<ADDTWOINTS-REQUEST>"
   "ADDTWOINTS-REQUEST"
   "<ADDTWOINTS-RESPONSE>"
   "ADDTWOINTS-RESPONSE"
   "SPEECHTOTEXT"
   "<SPEECHTOTEXT-REQUEST>"
   "SPEECHTOTEXT-REQUEST"
   "<SPEECHTOTEXT-RESPONSE>"
   "SPEECHTOTEXT-RESPONSE"
  ))

