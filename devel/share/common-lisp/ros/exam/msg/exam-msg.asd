
(cl:in-package :asdf)

(defsystem "exam-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :actionlib_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "PersonAction" :depends-on ("_package_PersonAction"))
    (:file "_package_PersonAction" :depends-on ("_package"))
    (:file "PersonActionFeedback" :depends-on ("_package_PersonActionFeedback"))
    (:file "_package_PersonActionFeedback" :depends-on ("_package"))
    (:file "PersonActionGoal" :depends-on ("_package_PersonActionGoal"))
    (:file "_package_PersonActionGoal" :depends-on ("_package"))
    (:file "PersonActionResult" :depends-on ("_package_PersonActionResult"))
    (:file "_package_PersonActionResult" :depends-on ("_package"))
    (:file "PersonFeedback" :depends-on ("_package_PersonFeedback"))
    (:file "_package_PersonFeedback" :depends-on ("_package"))
    (:file "PersonGoal" :depends-on ("_package_PersonGoal"))
    (:file "_package_PersonGoal" :depends-on ("_package"))
    (:file "PersonResult" :depends-on ("_package_PersonResult"))
    (:file "_package_PersonResult" :depends-on ("_package"))
  ))