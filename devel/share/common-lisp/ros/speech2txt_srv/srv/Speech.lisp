; Auto-generated. Do not edit!


(cl:in-package speech2txt_srv-srv)


;//! \htmlinclude Speech-request.msg.html

(cl:defclass <Speech-request> (roslisp-msg-protocol:ros-message)
  ((a
    :reader a
    :initarg :a
    :type cl:string
    :initform ""))
)

(cl:defclass Speech-request (<Speech-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Speech-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Speech-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name speech2txt_srv-srv:<Speech-request> is deprecated: use speech2txt_srv-srv:Speech-request instead.")))

(cl:ensure-generic-function 'a-val :lambda-list '(m))
(cl:defmethod a-val ((m <Speech-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader speech2txt_srv-srv:a-val is deprecated.  Use speech2txt_srv-srv:a instead.")
  (a m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Speech-request>) ostream)
  "Serializes a message object of type '<Speech-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'a))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'a))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Speech-request>) istream)
  "Deserializes a message object of type '<Speech-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'a) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'a) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Speech-request>)))
  "Returns string type for a service object of type '<Speech-request>"
  "speech2txt_srv/SpeechRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Speech-request)))
  "Returns string type for a service object of type 'Speech-request"
  "speech2txt_srv/SpeechRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Speech-request>)))
  "Returns md5sum for a message object of type '<Speech-request>"
  "ab61c62fb399f4eebf59b55f633add2f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Speech-request)))
  "Returns md5sum for a message object of type 'Speech-request"
  "ab61c62fb399f4eebf59b55f633add2f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Speech-request>)))
  "Returns full string definition for message of type '<Speech-request>"
  (cl:format cl:nil "string a~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Speech-request)))
  "Returns full string definition for message of type 'Speech-request"
  (cl:format cl:nil "string a~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Speech-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'a))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Speech-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Speech-request
    (cl:cons ':a (a msg))
))
;//! \htmlinclude Speech-response.msg.html

(cl:defclass <Speech-response> (roslisp-msg-protocol:ros-message)
  ((analysis
    :reader analysis
    :initarg :analysis
    :type cl:string
    :initform ""))
)

(cl:defclass Speech-response (<Speech-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Speech-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Speech-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name speech2txt_srv-srv:<Speech-response> is deprecated: use speech2txt_srv-srv:Speech-response instead.")))

(cl:ensure-generic-function 'analysis-val :lambda-list '(m))
(cl:defmethod analysis-val ((m <Speech-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader speech2txt_srv-srv:analysis-val is deprecated.  Use speech2txt_srv-srv:analysis instead.")
  (analysis m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Speech-response>) ostream)
  "Serializes a message object of type '<Speech-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'analysis))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'analysis))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Speech-response>) istream)
  "Deserializes a message object of type '<Speech-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'analysis) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'analysis) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Speech-response>)))
  "Returns string type for a service object of type '<Speech-response>"
  "speech2txt_srv/SpeechResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Speech-response)))
  "Returns string type for a service object of type 'Speech-response"
  "speech2txt_srv/SpeechResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Speech-response>)))
  "Returns md5sum for a message object of type '<Speech-response>"
  "ab61c62fb399f4eebf59b55f633add2f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Speech-response)))
  "Returns md5sum for a message object of type 'Speech-response"
  "ab61c62fb399f4eebf59b55f633add2f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Speech-response>)))
  "Returns full string definition for message of type '<Speech-response>"
  (cl:format cl:nil "string analysis~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Speech-response)))
  "Returns full string definition for message of type 'Speech-response"
  (cl:format cl:nil "string analysis~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Speech-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'analysis))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Speech-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Speech-response
    (cl:cons ':analysis (analysis msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Speech)))
  'Speech-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Speech)))
  'Speech-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Speech)))
  "Returns string type for a service object of type '<Speech>"
  "speech2txt_srv/Speech")