;; Auto-generated. Do not edit!


(when (boundp 'actionlib_tutorials::SpeechGoal)
  (if (not (find-package "ACTIONLIB_TUTORIALS"))
    (make-package "ACTIONLIB_TUTORIALS"))
  (shadow 'SpeechGoal (find-package "ACTIONLIB_TUTORIALS")))
(unless (find-package "ACTIONLIB_TUTORIALS::SPEECHGOAL")
  (make-package "ACTIONLIB_TUTORIALS::SPEECHGOAL"))

(in-package "ROS")
;;//! \htmlinclude SpeechGoal.msg.html


(defclass actionlib_tutorials::SpeechGoal
  :super ros::object
  :slots (_order ))

(defmethod actionlib_tutorials::SpeechGoal
  (:init
   (&key
    ((:order __order) 0)
    )
   (send-super :init)
   (setq _order (round __order))
   self)
  (:order
   (&optional __order)
   (if __order (setq _order __order)) _order)
  (:serialization-length
   ()
   (+
    ;; int32 _order
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _order
       (write-long _order s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _order
     (setq _order (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get actionlib_tutorials::SpeechGoal :md5sum-) "6889063349a00b249bd1661df429d822")
(setf (get actionlib_tutorials::SpeechGoal :datatype-) "actionlib_tutorials/SpeechGoal")
(setf (get actionlib_tutorials::SpeechGoal :definition-)
      "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======
#goal definition
int32 order

")



(provide :actionlib_tutorials/SpeechGoal "6889063349a00b249bd1661df429d822")


