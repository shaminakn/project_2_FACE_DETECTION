;; Auto-generated. Do not edit!


(when (boundp 'speech2txt_srv::SpeechToText)
  (if (not (find-package "SPEECH2TXT_SRV"))
    (make-package "SPEECH2TXT_SRV"))
  (shadow 'SpeechToText (find-package "SPEECH2TXT_SRV")))
(unless (find-package "SPEECH2TXT_SRV::SPEECHTOTEXT")
  (make-package "SPEECH2TXT_SRV::SPEECHTOTEXT"))
(unless (find-package "SPEECH2TXT_SRV::SPEECHTOTEXTREQUEST")
  (make-package "SPEECH2TXT_SRV::SPEECHTOTEXTREQUEST"))
(unless (find-package "SPEECH2TXT_SRV::SPEECHTOTEXTRESPONSE")
  (make-package "SPEECH2TXT_SRV::SPEECHTOTEXTRESPONSE"))

(in-package "ROS")





(defclass speech2txt_srv::SpeechToTextRequest
  :super ros::object
  :slots (_a _b ))

(defmethod speech2txt_srv::SpeechToTextRequest
  (:init
   (&key
    ((:a __a) 0)
    ((:b __b) 0)
    )
   (send-super :init)
   (setq _a (round __a))
   (setq _b (round __b))
   self)
  (:a
   (&optional __a)
   (if __a (setq _a __a)) _a)
  (:b
   (&optional __b)
   (if __b (setq _b __b)) _b)
  (:serialization-length
   ()
   (+
    ;; int64 _a
    8
    ;; int64 _b
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _a
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _a (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _a) (= (length (_a . bv)) 2)) ;; bignum
              (write-long (ash (elt (_a . bv) 0) 0) s)
              (write-long (ash (elt (_a . bv) 1) -1) s))
             ((and (class _a) (= (length (_a . bv)) 1)) ;; big1
              (write-long (elt (_a . bv) 0) s)
              (write-long (if (>= _a 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _a s)(write-long (if (>= _a 0) 0 #xffffffff) s)))
     ;; int64 _b
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _b (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _b) (= (length (_b . bv)) 2)) ;; bignum
              (write-long (ash (elt (_b . bv) 0) 0) s)
              (write-long (ash (elt (_b . bv) 1) -1) s))
             ((and (class _b) (= (length (_b . bv)) 1)) ;; big1
              (write-long (elt (_b . bv) 0) s)
              (write-long (if (>= _b 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _b s)(write-long (if (>= _b 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _a
#+(or :alpha :irix6 :x86_64)
      (setf _a (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _a (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; int64 _b
#+(or :alpha :irix6 :x86_64)
      (setf _b (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _b (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(defclass speech2txt_srv::SpeechToTextResponse
  :super ros::object
  :slots (_sum ))

(defmethod speech2txt_srv::SpeechToTextResponse
  (:init
   (&key
    ((:sum __sum) 0)
    )
   (send-super :init)
   (setq _sum (round __sum))
   self)
  (:sum
   (&optional __sum)
   (if __sum (setq _sum __sum)) _sum)
  (:serialization-length
   ()
   (+
    ;; int64 _sum
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _sum
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _sum (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _sum) (= (length (_sum . bv)) 2)) ;; bignum
              (write-long (ash (elt (_sum . bv) 0) 0) s)
              (write-long (ash (elt (_sum . bv) 1) -1) s))
             ((and (class _sum) (= (length (_sum . bv)) 1)) ;; big1
              (write-long (elt (_sum . bv) 0) s)
              (write-long (if (>= _sum 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _sum s)(write-long (if (>= _sum 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _sum
#+(or :alpha :irix6 :x86_64)
      (setf _sum (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _sum (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(defclass speech2txt_srv::SpeechToText
  :super ros::object
  :slots ())

(setf (get speech2txt_srv::SpeechToText :md5sum-) "6a2e34150c00229791cc89ff309fff21")
(setf (get speech2txt_srv::SpeechToText :datatype-) "speech2txt_srv/SpeechToText")
(setf (get speech2txt_srv::SpeechToText :request) speech2txt_srv::SpeechToTextRequest)
(setf (get speech2txt_srv::SpeechToText :response) speech2txt_srv::SpeechToTextResponse)

(defmethod speech2txt_srv::SpeechToTextRequest
  (:response () (instance speech2txt_srv::SpeechToTextResponse :init)))

(setf (get speech2txt_srv::SpeechToTextRequest :md5sum-) "6a2e34150c00229791cc89ff309fff21")
(setf (get speech2txt_srv::SpeechToTextRequest :datatype-) "speech2txt_srv/SpeechToTextRequest")
(setf (get speech2txt_srv::SpeechToTextRequest :definition-)
      "int64 a
int64 b
---
int64 sum

")

(setf (get speech2txt_srv::SpeechToTextResponse :md5sum-) "6a2e34150c00229791cc89ff309fff21")
(setf (get speech2txt_srv::SpeechToTextResponse :datatype-) "speech2txt_srv/SpeechToTextResponse")
(setf (get speech2txt_srv::SpeechToTextResponse :definition-)
      "int64 a
int64 b
---
int64 sum

")



(provide :speech2txt_srv/SpeechToText "6a2e34150c00229791cc89ff309fff21")


