;; Auto-generated. Do not edit!


(when (boundp 'speech2txt_srv::Speech)
  (if (not (find-package "SPEECH2TXT_SRV"))
    (make-package "SPEECH2TXT_SRV"))
  (shadow 'Speech (find-package "SPEECH2TXT_SRV")))
(unless (find-package "SPEECH2TXT_SRV::SPEECH")
  (make-package "SPEECH2TXT_SRV::SPEECH"))
(unless (find-package "SPEECH2TXT_SRV::SPEECHREQUEST")
  (make-package "SPEECH2TXT_SRV::SPEECHREQUEST"))
(unless (find-package "SPEECH2TXT_SRV::SPEECHRESPONSE")
  (make-package "SPEECH2TXT_SRV::SPEECHRESPONSE"))

(in-package "ROS")





(defclass speech2txt_srv::SpeechRequest
  :super ros::object
  :slots (_a ))

(defmethod speech2txt_srv::SpeechRequest
  (:init
   (&key
    ((:a __a) "")
    )
   (send-super :init)
   (setq _a (string __a))
   self)
  (:a
   (&optional __a)
   (if __a (setq _a __a)) _a)
  (:serialization-length
   ()
   (+
    ;; string _a
    4 (length _a)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _a
       (write-long (length _a) s) (princ _a s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _a
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _a (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass speech2txt_srv::SpeechResponse
  :super ros::object
  :slots (_analysis ))

(defmethod speech2txt_srv::SpeechResponse
  (:init
   (&key
    ((:analysis __analysis) "")
    )
   (send-super :init)
   (setq _analysis (string __analysis))
   self)
  (:analysis
   (&optional __analysis)
   (if __analysis (setq _analysis __analysis)) _analysis)
  (:serialization-length
   ()
   (+
    ;; string _analysis
    4 (length _analysis)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _analysis
       (write-long (length _analysis) s) (princ _analysis s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _analysis
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _analysis (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass speech2txt_srv::Speech
  :super ros::object
  :slots ())

(setf (get speech2txt_srv::Speech :md5sum-) "ab61c62fb399f4eebf59b55f633add2f")
(setf (get speech2txt_srv::Speech :datatype-) "speech2txt_srv/Speech")
(setf (get speech2txt_srv::Speech :request) speech2txt_srv::SpeechRequest)
(setf (get speech2txt_srv::Speech :response) speech2txt_srv::SpeechResponse)

(defmethod speech2txt_srv::SpeechRequest
  (:response () (instance speech2txt_srv::SpeechResponse :init)))

(setf (get speech2txt_srv::SpeechRequest :md5sum-) "ab61c62fb399f4eebf59b55f633add2f")
(setf (get speech2txt_srv::SpeechRequest :datatype-) "speech2txt_srv/SpeechRequest")
(setf (get speech2txt_srv::SpeechRequest :definition-)
      "string a

---
string analysis

")

(setf (get speech2txt_srv::SpeechResponse :md5sum-) "ab61c62fb399f4eebf59b55f633add2f")
(setf (get speech2txt_srv::SpeechResponse :datatype-) "speech2txt_srv/SpeechResponse")
(setf (get speech2txt_srv::SpeechResponse :definition-)
      "string a

---
string analysis

")



(provide :speech2txt_srv/Speech "ab61c62fb399f4eebf59b55f633add2f")


