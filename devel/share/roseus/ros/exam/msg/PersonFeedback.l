;; Auto-generated. Do not edit!


(when (boundp 'exam::PersonFeedback)
  (if (not (find-package "EXAM"))
    (make-package "EXAM"))
  (shadow 'PersonFeedback (find-package "EXAM")))
(unless (find-package "EXAM::PERSONFEEDBACK")
  (make-package "EXAM::PERSONFEEDBACK"))

(in-package "ROS")
;;//! \htmlinclude PersonFeedback.msg.html


(defclass exam::PersonFeedback
  :super ros::object
  :slots (_sequence ))

(defmethod exam::PersonFeedback
  (:init
   (&key
    ((:sequence __sequence) 0)
    )
   (send-super :init)
   (setq _sequence (round __sequence))
   self)
  (:sequence
   (&optional __sequence)
   (if __sequence (setq _sequence __sequence)) _sequence)
  (:serialization-length
   ()
   (+
    ;; int32 _sequence
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _sequence
       (write-long _sequence s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _sequence
     (setq _sequence (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get exam::PersonFeedback :md5sum-) "58626a12c49e3af2868525eb06998e08")
(setf (get exam::PersonFeedback :datatype-) "exam/PersonFeedback")
(setf (get exam::PersonFeedback :definition-)
      "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======
#feedback
int32 sequence


")



(provide :exam/PersonFeedback "58626a12c49e3af2868525eb06998e08")


