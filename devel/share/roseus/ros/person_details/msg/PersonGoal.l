;; Auto-generated. Do not edit!


(when (boundp 'person_details::PersonGoal)
  (if (not (find-package "PERSON_DETAILS"))
    (make-package "PERSON_DETAILS"))
  (shadow 'PersonGoal (find-package "PERSON_DETAILS")))
(unless (find-package "PERSON_DETAILS::PERSONGOAL")
  (make-package "PERSON_DETAILS::PERSONGOAL"))

(in-package "ROS")
;;//! \htmlinclude PersonGoal.msg.html


(defclass person_details::PersonGoal
  :super ros::object
  :slots (_namz ))

(defmethod person_details::PersonGoal
  (:init
   (&key
    ((:namz __namz) "")
    )
   (send-super :init)
   (setq _namz (string __namz))
   self)
  (:namz
   (&optional __namz)
   (if __namz (setq _namz __namz)) _namz)
  (:serialization-length
   ()
   (+
    ;; string _namz
    4 (length _namz)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _namz
       (write-long (length _namz) s) (princ _namz s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _namz
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _namz (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get person_details::PersonGoal :md5sum-) "ddb26cb5924a342c07ea9f41c7081616")
(setf (get person_details::PersonGoal :datatype-) "person_details/PersonGoal")
(setf (get person_details::PersonGoal :definition-)
      "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======
#goal definition
string namz

")



(provide :person_details/PersonGoal "ddb26cb5924a342c07ea9f41c7081616")


