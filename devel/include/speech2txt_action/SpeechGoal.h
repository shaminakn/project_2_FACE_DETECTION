// Generated by gencpp from file speech2txt_action/SpeechGoal.msg
// DO NOT EDIT!


#ifndef SPEECH2TXT_ACTION_MESSAGE_SPEECHGOAL_H
#define SPEECH2TXT_ACTION_MESSAGE_SPEECHGOAL_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace speech2txt_action
{
template <class ContainerAllocator>
struct SpeechGoal_
{
  typedef SpeechGoal_<ContainerAllocator> Type;

  SpeechGoal_()
    : text()  {
    }
  SpeechGoal_(const ContainerAllocator& _alloc)
    : text(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _text_type;
  _text_type text;





  typedef boost::shared_ptr< ::speech2txt_action::SpeechGoal_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::speech2txt_action::SpeechGoal_<ContainerAllocator> const> ConstPtr;

}; // struct SpeechGoal_

typedef ::speech2txt_action::SpeechGoal_<std::allocator<void> > SpeechGoal;

typedef boost::shared_ptr< ::speech2txt_action::SpeechGoal > SpeechGoalPtr;
typedef boost::shared_ptr< ::speech2txt_action::SpeechGoal const> SpeechGoalConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::speech2txt_action::SpeechGoal_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace speech2txt_action

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'speech2txt_action': ['/home/shamina/catkin_ws/devel/share/speech2txt_action/msg'], 'actionlib_msgs': ['/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::speech2txt_action::SpeechGoal_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::speech2txt_action::SpeechGoal_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::speech2txt_action::SpeechGoal_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
{
  static const char* value()
  {
    return "74697ed3d931f6eede8bf3a8dfeca160";
  }

  static const char* value(const ::speech2txt_action::SpeechGoal_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x74697ed3d931f6eeULL;
  static const uint64_t static_value2 = 0xde8bf3a8dfeca160ULL;
};

template<class ContainerAllocator>
struct DataType< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
{
  static const char* value()
  {
    return "speech2txt_action/SpeechGoal";
  }

  static const char* value(const ::speech2txt_action::SpeechGoal_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n\
#goal definition\n\
string text\n\
";
  }

  static const char* value(const ::speech2txt_action::SpeechGoal_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.text);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct SpeechGoal_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::speech2txt_action::SpeechGoal_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::speech2txt_action::SpeechGoal_<ContainerAllocator>& v)
  {
    s << indent << "text: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.text);
  }
};

} // namespace message_operations
} // namespace ros

#endif // SPEECH2TXT_ACTION_MESSAGE_SPEECHGOAL_H
